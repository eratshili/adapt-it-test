angular
    .module('app.config', [])
    .config(config)
    .run(run);

    config.$inject = ['$stateProvider', '$httpProvider', '$locationProvider', '$urlRouterProvider'];

    function config($stateProvider, $httpProvider, $locationProvider, $urlRouterProvider) {

        $stateProvider
        .state('users', {
            url: '/',
            templateUrl: 'users/views/users.html',
            controller: "UsersCtrl",
            controllerAs: 'vm'
        })
        .state('user-details', {
            url: '/user-details/:id',
            templateUrl: 'users/views/user_details.html',
            controller: "UserDetailsCtrl",
            controllerAs: 'vm',
            showBackButton: true
        });
        
        $urlRouterProvider.otherwise('/');
        $locationProvider.hashPrefix('');
    }

    // Run starts here... 
    run.$inject = ['$rootScope', '$window'];

    function run($rootScope, $window) {
        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            $rootScope.showBackButton = (next.indexOf("user-details") >= 0) ? true : false;
        });
        $rootScope.$on('$locationChangeSuccess', function() {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            $window.scrollTo(0, 0);
        });
    }