angular
  .module('app.user-details', [])
  .controller('UserDetailsCtrl', UserDetailsCtrl);

UserDetailsCtrl.$inject = ['$stateParams', 'userService'];

function UserDetailsCtrl($stateParams, userService) {
  var vm = this;
  var id = $stateParams.id;
  var barOption = {
    color: '#aaa',
    strokeWidth: 4,
    trailWidth: 4,
    easing: 'easeInOut',
    duration: 1400,
    text: { autoStyleContainer: false },
    from: { color: '#aaa', width: 4 },
    to: { color: '#8cc63e', width: 4 },
    step: function (state, circle) {
      circle.path.setAttribute('stroke', state.color);
      circle.path.setAttribute('stroke-width', state.width);

      if(circle._container.id === 'minutes'){
        circle.setText('<span class="h3">'+ vm.user.remaining_voice_minutes + 'min</span><br>of ' + vm.user.allocated_voice_minutes +'min');
      }
      else if(circle._container.id === 'data'){
        circle.setText('<span class="h3">'+ vm.user.remaining_data_mb + 'MB</span><br>of ' + vm.user.allocated_data_mb+'MB');
      }
      else {
        circle.setText('<span class="h3">'+ vm.user.remaining_messages + 'SMS\'s</span><br>of '+ vm.user.allocated_messages+'SMS\'s');
      }
    }
  };

  userService.getUserDetails(id).then(function(response){
    vm.user = response;
    
    // creating circles
    var minutesBar = new ProgressBar.Circle(minutes, barOption);
    var dataBar = new ProgressBar.Circle(data, barOption);
    var messageBar = new ProgressBar.Circle(messages, barOption);
    
    minutesBar.animate(getPercentageBalances(vm.user.allocated_voice_minutes, vm.user.remaining_voice_minutes)/ 100);
    dataBar.animate(getPercentageBalances(vm.user.allocated_data_mb, vm.user.remaining_data_mb)/ 100);
    messageBar.animate(getPercentageBalances(vm.user.allocated_messages, vm.user.remaining_messages)/ 100);
  });

  // Returns balance in percentage
  function getPercentageBalances(allocated, remaining){
    return ((remaining * 100) / allocated).toFixed(2);
  }
}
