angular
  .module('app.users', [])
  .controller('UsersCtrl', UsersCtrl);

UsersCtrl.$inject = ['$state', 'userService'];

function UsersCtrl($state, userService) {
  var vm = this;
  userService.getAllUsers().then(function(response) {
      vm.users = response.data;
  }, function(err){
    console.log(err);
  });

  vm.goToUser = function(id){
    $state.go('user-details', { id: id });
  };
}
