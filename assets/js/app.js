  angular.module('acmeApp', [
    'ui.router',
    'ngAnimate',
    
    'app.config',
    'app.controller',
    'app.users',
    'app.user-details',
    'app.user-service'
  ]);