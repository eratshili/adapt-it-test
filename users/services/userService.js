angular
    .module('app.user-service', [])
    .factory('userService', userService);

userService.$inject = ['$http'];

function userService ($http) {
  var users = [];
	return {
    getAllUsers: getAllUsers,
    getUserDetails: getUserDetails
  };

  function getAllUsers() {
    return $http.get('assets/example_data/user-list.json');
  }

  function getUserDetails(id) {
    return $http.get('assets/example_data/user-detail.json').then(function(response){
      return _.find(response.data, {id: id});
    });
  }
}