# README #

This is an exercise project given to Eric Ratshili by Adatp IT

### How do I get set up? ###

* run **bower install**
* then install **npm install http-server -g** and run **http-server** from the project directory
* To open the app go to **http://localhost:8080**