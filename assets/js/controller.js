angular
    .module('app.controller', [])
    .controller('AppCtrl', AppCtrl);

AppCtrl.$inject = ['$scope'];

function AppCtrl($scope) {
  var today = new Date();
  $scope.year = today.getFullYear();
}
